using UnityEngine;

/*! \class LevelCombatOptions
 *  \brief Has the combats line ups availables in a level.
 */
[CreateAssetMenu(fileName = "New Level Combats", menuName = "Graciosa/Level Combats", order = 0)]
public class LevelCombatOptions : ScriptableObject
{   
    public float EasyCombatChance;                                      //!< Percentage chance to generate an easy combat.
    public float NormalCombatChance;                                    //!< Percentage chance to generate an normal combat.
    public float HardCombatChance;                                      //!< Percentage chance to generate an hard combat.

    public EnemiesBaseStatus DefaultStatus;                             //!< Enemies default base status.

    public CombatGroups[] EasyCombats;                                  //!< Easy combats line up.
    public CombatGroups[] NormalCombats;                                //!< Normal combats line up.
    public CombatGroups[] HardCombats;                                  //!< Hard combats line up.
}
