using UnityEngine;
using System.Collections.Generic;

/*! \class StrongestTargetBehavior
*  \brief Tragets the Strongest player character.
*/
[CreateAssetMenu(fileName = "StrongestTarget", menuName = "Graciosa/Combat AI/Target Selector/Strongest Target Selector", order = 4)]
public class StrongestTargetBehavior : AiTargetSelector
{
    /// Finds the character with higher life amount.
    private int FindStrongestCharacter (List<FightingCharacter> characters)
    {
        int characterIndex = 0;
        int actualCharacterHealth = 0;
        int higherLife = characters[0].GetHealth();

        for (int i = 1; i < characters.Count; i++)
        {
            actualCharacterHealth = characters[i].GetHealth();

            if (actualCharacterHealth > higherLife)
            {
                higherLife = actualCharacterHealth;
                characterIndex = i;
            }
        }

        return characterIndex;
    }

    /// <summary>
    /// Selects the targets.
    /// </summary> 
    public override List<FightingCharacter> Act (AttackTargets attackTargets)
    {
        List<FightingCharacter> selectedTargets = new List<FightingCharacter>();            

        List<FightingCharacter> tempTargets = new List<FightingCharacter>();

        if (attackTargets == AttackTargets.SinglePlayer)
        {
            // Get all player characters.
        }
        else
        {
            // Get all enemies characters.
        }

        int characterIndex = FindStrongestCharacter(tempTargets);            

        selectedTargets.Add(tempTargets[characterIndex]);

        return selectedTargets;
    }

}

