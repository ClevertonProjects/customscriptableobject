using System.Collections.Generic;
using UnityEngine;

/*! \class TargetRandom
*  \brief Targets a random character.
*/
[CreateAssetMenu(fileName = "TargetRandom", menuName = "Graciosa/Combat AI/Target Selector/Target Random", order = 6)]
public class TargetRandom : AiTargetSelector
{
    /// <summary>
    /// Selects the targets.
    /// </summary> 
    public override List<FightingCharacter> Act (AttackTargets attackTargets)
    {
        List<FightingCharacter> selectedTargets = new List<FightingCharacter>();
        List<FightingCharacter> tempCharacters = new List<FightingCharacter>();

        if (attackTargets == AttackTargets.SinglePlayer)
        {
            // Get all player characters.
        }
        else
        {
            // Get all enemies characters.
        }

        selectedTargets.Add(tempCharacters[Random.Range(0, tempCharacters.Count)]);
        return selectedTargets;
    }
}

