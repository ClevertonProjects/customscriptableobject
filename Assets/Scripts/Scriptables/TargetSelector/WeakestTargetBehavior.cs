using System.Collections.Generic;
using UnityEngine;

/*! \class WeakestTargetBehavior
*  \brief Targets the weakest player character.
*/
[CreateAssetMenu(fileName = "WeakestTarget", menuName = "Graciosa/Combat AI/Target Selector/Weakest Target Selector", order = 3)]
public class WeakestTargetBehavior : AiTargetSelector
{
    /// Finds the character with lesser life amount.
    private int FindWeakestCharacter (List<FightingCharacter> characters)
    {
        int weakestCharacterIndex = 0;
        float actualCharacterHealth = 1f;
        float lesserLife = characters[0].GetHealthProportion();

        for (int i = 1; i < characters.Count; i++)
        {
            actualCharacterHealth = characters[i].GetHealthProportion();
            if (characters[i].IsDead()) continue;

            if (actualCharacterHealth < lesserLife)
            {
                lesserLife = actualCharacterHealth;
                weakestCharacterIndex = i;
            }
        }

        return weakestCharacterIndex;
    }        

    /// <summary>
    /// Selects the targets.
    /// </summary> 
    public override List<FightingCharacter> Act (AttackTargets attackTargets)
    {
        List<FightingCharacter> selectedTargets = new List<FightingCharacter>();
        List<FightingCharacter> tempTargets = new List<FightingCharacter>();

        if (attackTargets == AttackTargets.SinglePlayer)
        {
            // Get all player characters.
        }
        else
        {
            // Get all enemy characters.
        }

        int weakestCharacterIndex = FindWeakestCharacter(tempTargets);            

        selectedTargets.Add(tempTargets[weakestCharacterIndex]);

        return selectedTargets;
    }
}

