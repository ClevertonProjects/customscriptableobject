using UnityEngine;
using System.Collections.Generic;

/*! \class AiTargetSelector
*  \brief Defines the AI behavior to select attack targets.
*/
public class AiTargetSelector : ScriptableObject
{
    /// <summary>
    /// Selects the attack targets.
    /// </summary>        
    public virtual List<FightingCharacter> Act (AttackTargets attackTargets)
    {            
        return null;
    }
}

