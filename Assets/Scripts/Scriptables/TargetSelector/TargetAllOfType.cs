using System.Collections.Generic;
using UnityEngine;

/*! \class TargetAllOfType
*  \brief Targets all characters of a type.
*/
[CreateAssetMenu(fileName = "TargetAll", menuName = "Graciosa/Combat AI/Target Selector/Target All", order = 5)]
public class TargetAllOfType : AiTargetSelector
{
    /// <summary>
    /// Selects the targets.
    /// </summary> 
    public override List<FightingCharacter> Act (AttackTargets attackTargets)
    {
        List<FightingCharacter> selectedTargets = new List<FightingCharacter>();

        if (attackTargets == AttackTargets.SinglePlayer || attackTargets == AttackTargets.MultiplePlayers)
        {
            // Get all player characters.
        }
        else
        {
            // Get all enemies characters.
        }            

        return selectedTargets;
    }
}
