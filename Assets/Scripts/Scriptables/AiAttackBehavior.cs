using UnityEngine;
using System.Collections.Generic;

/*! \class AiAttackBehavior
*  \brief Base class for NPCs attack behaviors.
*/
[CreateAssetMenu(fileName = "New AI Behavior", menuName = "Graciosa/Combat AI/AI Behavior", order = 2)]
public class AiAttackBehavior : ScriptableObject
{
    [SerializeField] private BaseAttack[] attacks;                                  //!< Character's attacks.
    private int selectedAttackIndex;                                                //!< Index of the selected attack.

    [SerializeField] private AttackSelectionParam attackSelectionParam;             //!< The parameter type used to select an attack.
    [SerializeField] private float[] selectionParameterThreshold;                   //!< Threshold values for attack selection.

    [SerializeField] private AiTargetSelector[] targetSelectors;                    //!< Target's selection behavior for attacks.        
        
    public bool IsAllyHealthBinded
    {
        get { return attackSelectionParam == AttackSelectionParam.WeakestAllyHealth; }
    }

    /// <summary>
    /// Selects the attack targets.
    /// </summary>        
    public List<FightingCharacter> SelectTargets ()
    {
        List<FightingCharacter> selectedTargets = targetSelectors[selectedAttackIndex].Act(attacks[selectedAttackIndex].AttackTargets);

        return selectedTargets;
    }        

    /// Get the attack index corresponding to the selection threshold.
    private int GetAttackIndexByLife (float lifeProportion)
    {
        int attackIndex = 0;

        foreach (float threshold in selectionParameterThreshold)
        {
            if (lifeProportion >= threshold)
            {
                return attackIndex;
            }

            attackIndex++;
        }            

        return attackIndex - 1;
    }        

    /// Get an attack index randomly.
    private int GetAttackIndexRandomly ()
    {
        int attackIndex = Random.Range(0, attacks.Length);
        return attackIndex;
    }

    /// Get an attack index selected from a random value.
    private int GetAttackIndexByRandomThreshold ()
    {
        float min = selectionParameterThreshold[selectionParameterThreshold.Length - 1];
        float max = selectionParameterThreshold[0];
        float randomValue = Random.Range(min, max);

        int attackIndex = 0;

        foreach (float threshold in selectionParameterThreshold)
        {
            if (randomValue >= threshold)
            {
                return attackIndex;
            }

            attackIndex++;
        }

        return attackIndex - 1;
    }

    /// Check if the selected attack is valid.
    private void ValidateSelectedAttack ()
    {
        int attacksLength = attacks.Length;
        int actualLoopAmount = 0;   // Control variable used to avoid an infinite loop.
            
        actualLoopAmount++;
        selectedAttackIndex++;

        if (selectedAttackIndex >= attacksLength)
        {
            selectedAttackIndex = 0;
        }

        if (actualLoopAmount >= attacksLength)
        {
            // When all the attacks are invalid, select the first as default and continue the game.
            selectedAttackIndex = 0;               
        }
            
    }

    /// Gets an attack based on the selection parameter.
    private BaseAttack GetAttackByParameter (float selectionParameter)
    {
        if (attacks.Length == 1)
        {
            return attacks[0];
        }

        switch (attackSelectionParam)
        {
            case AttackSelectionParam.WeakestAllyHealth:
            case AttackSelectionParam.OwnHealth:
                selectedAttackIndex = GetAttackIndexByLife(selectionParameter);
                break;

            case AttackSelectionParam.RandomFromThreshold:
                selectedAttackIndex = GetAttackIndexByRandomThreshold();
                break;

            case AttackSelectionParam.RandomAttack:
                selectedAttackIndex = GetAttackIndexRandomly();
                break;

            default:
                break;
        }

        ValidateSelectedAttack();            

        return attacks[selectedAttackIndex];            
    }

    /// <summary>
    /// Selects an attack.
    /// </summary>        
    public BaseAttack SelectAttack (float selectionParameter)
    {
        BaseAttack selectedAttack = GetAttackByParameter(selectionParameter);
        return selectedAttack;
    }
}

