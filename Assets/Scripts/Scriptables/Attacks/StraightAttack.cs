using UnityEngine;
using System.Collections.Generic;

/*! \class StaightAttack
*  \brief Has the data of a direct attack that causes damage to the target.
*/
[CreateAssetMenu(fileName = "Straight Attack", menuName = "Graciosa/Attacks/Straight Attack", order = 1)]
public class StraightAttack : BaseAttack
{
    public int Damage;                                          //!< Attack damage.

    /// <summary>
    /// Executes the character's attack.
    /// </summary>
    public override void Attack (List<FightingCharacter> targets, FightingCharacter attacker)
    {            
        foreach (FightingCharacter target in targets)
        {
            target.ApplyDamage(Damage);
        }            
    }
}

