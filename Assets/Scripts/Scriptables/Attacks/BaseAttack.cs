using UnityEngine;
using System.Collections.Generic;

/*! \class BaseAttack
*  \brief Base class used to create characters' attacks.
*/    
public class BaseAttack : ScriptableObject
{
    public AttackTargets AttackTargets;                         //!< Type of expected attack targets.   
               
    /// <summary>
    /// Executes the character's attack.
    /// </summary>
    public virtual void Attack (List<FightingCharacter> targets, FightingCharacter attacker)
    {

    }
}

