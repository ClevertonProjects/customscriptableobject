using UnityEngine;

/*! \class EnemiesBaseStatus
*  \brief Has the enemies' base combat data.
*/    
public class EnemiesBaseStatus : ScriptableObject
{
    public EnemyParameters[] EnemyParameters;                          //!< Enemies' parameters values.                
}
