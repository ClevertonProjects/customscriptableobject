using UnityEngine;
using System;
using System.Collections.Generic;

/*! \class FightingCharacter
*  \brief Base class for combat characters.
*/
public class FightingCharacter : MonoBehaviour, IComparable<FightingCharacter>
{
    [SerializeField] protected int initialCombatInitiative;                 //!< Character's initial combat initiative value.
    protected int actualCombatInitiative;                                   //!< Actual value of combat initiative.        

    private List<FightingCharacter> targets = new List<FightingCharacter>();    //!< List of attack targets.         
       
    private void Start ()
    {
        actualCombatInitiative = initialCombatInitiative;            
    }

    /// <summary>
    /// Gets the character's initiave value.
    /// </summary>        
    public virtual int GetInitiative ()
    {
        return actualCombatInitiative;
    }  

    /// <summary>
    /// Sets the selected target.
    /// </summary>
    public void SetTarget (FightingCharacter selectedTarget)
    {
        targets.Add(selectedTarget);
    }

    /// <summary>
    /// Sets a group of targets.
    /// </summary>
    public void SetTarget (List<FightingCharacter> targetGroup)
    {
        targets = targetGroup;
    }

    /// <summary>
    /// Sorts two fighting characters by their initiative value.
    /// </summary>        
    public int CompareTo (FightingCharacter charToCompare)
    {
        int result = 0;
        int myInitiative = GetInitiative();
        int theirInitiative = charToCompare.GetInitiative();            

        if (myInitiative > theirInitiative)
        {                
            result = -1;
        }
        else if (myInitiative < theirInitiative)
        {                
            result = 1;
        }

        return result;
    }
        
    /// <summary>
    /// Executes the selected attack.
    /// </summary>        
    public virtual void DoAttack (BaseAttack baseAttack)
    {         
        baseAttack.Attack(targets, this);
    }        

    /// <summary>
    /// Applies damage to the character's life.
    /// </summary>        
    public virtual void ApplyDamage (int damage, bool isDefendable = true)
    {
            
    }   
        
    /// <summary>
    /// Gets the actual health value.
    /// </summary>        
    public virtual int GetHealth ()
    {
        return 0;
    }

    /// <summary>
    /// Gets the actual health proportion.
    /// </summary>        
    public virtual float GetHealthProportion ()
    {
        return 0f;
    }

    /// <summary>
    /// Check if the character is dead.
    /// </summary>        
    public virtual bool IsDead ()
    {
        return false;
    } 
}

