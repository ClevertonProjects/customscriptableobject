/*! \class CombatGroups
 *  \brief Sub class used to organize the combat categories.
 */
[System.Serializable]
public class CombatGroups
{
    public EnemyData[] CombatLineUp;                                //!< Stats of the enemies in this group.
    public int CombatWeight;                                        //!< Dificulty weight measure.
    public bool IsEditorPanelVisible;                               //!< Tracks the opening state of the editor fouldout.
}
