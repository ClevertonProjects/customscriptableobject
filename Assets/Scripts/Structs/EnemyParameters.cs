/*! \struct EnemyParameters
*  \brief Holds the enemy parameters data.
*/
[System.Serializable]
public struct EnemyParameters
{
    public EnemyTypes EnemyType;                        //!< Type of this enemy.
    public int LifeAmount;                              //!< Initial life amount.        
    public int Initiative;                              //!< Value used to define the attack order.        
    public AiAttackBehavior AiBehavior;                 //!< AI combat behavior.
}
