/*! \struct EnemyData
*  \brief Holds the enemy stats data.
*/
[System.Serializable]
public struct EnemyData
{
    public bool UseDefaultValues;                       //!< Check if the data used is from the default vaules file.
    public EnemyParameters Parameters;                  //!< Enemies parameters values.
}

