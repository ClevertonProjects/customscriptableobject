/*! AttackTargets enum, Lists the target types of the attacks. */
public enum AttackTargets
{
    SingleEnemy,        
    SinglePlayer,
    MultipleEnemies,
    MultiplePlayers,
}
