/*! AttackSelectionParam enum, Lists the types of parameters used to select an attack. */
public enum AttackSelectionParam
{
    OwnHealth,
    WeakestAllyHealth,
    RandomFromThreshold,
    RandomAttack,
}
