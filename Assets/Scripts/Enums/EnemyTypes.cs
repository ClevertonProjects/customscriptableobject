/*! EnemyTypes enum, Lists all the enemies' types in the game.*/
public enum EnemyTypes
{       
    Jaguar,
    Boar,
    Snake,
    Ocelot,
    Alligator,
    Tapir,
    Scarecrow
}
