using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelCombatOptions))]
public class LevelCombatInspector : Editor
{    
    LevelCombatOptions myTarget;
    SerializedObject mySerializedObject;   
    SerializedProperty easyCategoryData;
    SerializedProperty normalCategoryData;
    SerializedProperty hardCategoryData;
    SerializedProperty defaultStats;

    private bool showEasyPanel;
    private bool showNormalPanel;
    private bool showHardPanel;    

    private string groupsTitle = "Group ";
    private string easyCombats = "Easy Combats";
    private string normalCombats = "Normal Combats";
    private string hardCombats = "Hard Combats";

    GUIStyle titleStyle;
    
    void OnEnable ()
    {             
        Init();
    }

    public override void OnInspectorGUI()
    {     
        mySerializedObject.Update();        

        EditorGUILayout.LabelField("Combat Encounters", titleStyle);
        EditorGUILayout.Space();

        CombatDificultyChances();
        
        EditorGUILayout.Space();
                
        DrawCombatPanel(easyCategoryData, easyCombats, ref showEasyPanel);
        DrawCombatPanel(normalCategoryData, normalCombats, ref showNormalPanel);
        DrawCombatPanel(hardCategoryData, hardCombats, ref showHardPanel);

        mySerializedObject.ApplyModifiedProperties();
    }

    /// Draws the combat dificulties chance panel.
    private void CombatDificultyChances ()
    {
        defaultStats = mySerializedObject.FindProperty("DefaultStatus");

        SerializedProperty easyCombatChance = mySerializedObject.FindProperty("EasyCombatChance");
        SerializedProperty normalCombatChance = mySerializedObject.FindProperty("NormalCombatChance");
        SerializedProperty hardCombatChance = mySerializedObject.FindProperty("HardCombatChance");

        EditorGUILayout.ObjectField(defaultStats);
        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical("Box");

        EditorGUILayout.LabelField("Difficulty Chances", titleStyle);
        EditorGUILayout.BeginHorizontal();        

        EditorGUILayout.BeginVertical();        
        EditorGUILayout.LabelField("Easy", GUILayout.Width(100f));                
        EditorGUILayout.PropertyField(easyCombatChance, GUIContent.none);
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Normal", GUILayout.Width(100f));
        EditorGUILayout.PropertyField(normalCombatChance, GUIContent.none);        
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Hard", GUILayout.Width(100f));
        EditorGUILayout.PropertyField(hardCombatChance, GUIContent.none);        
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();
    }

    /// Draws the combat dificulty panel.
    private void DrawCombatPanel (SerializedProperty serializedProperty, string groupTitle, ref bool showPanel)
    {
        EditorGUILayout.BeginVertical("textfield");

        EditorGUILayout.BeginHorizontal();
        EditorGUI.indentLevel += 1;
        showPanel = EditorGUILayout.Foldout(showPanel, groupTitle);
        EditorGUILayout.EndHorizontal();

        if (showPanel)
        {
            DrawArraySizeProperty("Created Groups", serializedProperty);
            Color oldColor = GUI.backgroundColor;
            
            for (int i = 0; i < serializedProperty.arraySize; i++)
            {
                GUI.backgroundColor = new Color(1f, 0.996f, 0.894f);
                SerializedProperty lineUp = serializedProperty.GetArrayElementAtIndex(i);
                SerializedProperty combatGroups = lineUp.FindPropertyRelative("CombatLineUp");

                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.BeginHorizontal();

                SerializedProperty panelVisibilityProperty = serializedProperty.GetArrayElementAtIndex(i).FindPropertyRelative("IsEditorPanelVisible");
                panelVisibilityProperty.boolValue = EditorGUILayout.Foldout(panelVisibilityProperty.boolValue, groupsTitle + i);                

                EditorGUILayout.EndHorizontal();

                if (panelVisibilityProperty.boolValue)
                {           
                    SerializedProperty weightProperty = lineUp.FindPropertyRelative("CombatWeight");                    
                    EditorGUILayout.PropertyField(weightProperty);

                    DrawArraySizeProperty("Enemies Amount", combatGroups);

                    GUI.backgroundColor = new Color(0.937f, 0.933f, 1f);

                    for (int j = 0; j < combatGroups.arraySize; j++)
                    {
                        DrawEnemyData(combatGroups, j);
                    }
                }

                EditorGUILayout.EndVertical();
            }

            GUI.backgroundColor = oldColor;
        }

        EditorGUILayout.EndVertical();
        EditorGUI.indentLevel -= 1;
    }

    /// Insert elements into an array.
    private void AddArrayElements (int amount, SerializedProperty arrayProperty)
    {
        int arraySize = arrayProperty.arraySize;

        for (int i = 0; i < amount; i++)
        {
            arrayProperty.InsertArrayElementAtIndex(arraySize + i);
        }        
    }

    /// Removes some elements from an array.
    private void RemoveArrayElements (int amount, SerializedProperty arrayProperty)
    {
        int arraySize = arrayProperty.arraySize - 1;

        for (int i = 0; i < amount; i++)
        {
            arrayProperty.DeleteArrayElementAtIndex(arraySize - i);
        }
    }

    /// Draws the array size property.
    private void DrawArraySizeProperty (string labelContent, SerializedProperty arrayProperty)
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(labelContent);
        int actualSize = EditorGUILayout.IntField(arrayProperty.arraySize);

        EditorGUILayout.EndHorizontal();
        
        if (actualSize > arrayProperty.arraySize)
        {
            AddArrayElements(actualSize - arrayProperty.arraySize, arrayProperty);
        }
        else if (actualSize < arrayProperty.arraySize)
        {
            RemoveArrayElements(arrayProperty.arraySize - actualSize, arrayProperty);
        }
    }

    /// Draws the enemy data.
    private void DrawEnemyData (SerializedProperty combatGroup, int enemyIndex)
    {
        SerializedProperty enemyData = combatGroup.GetArrayElementAtIndex(enemyIndex);
        SerializedProperty enemyParameters = enemyData.FindPropertyRelative("Parameters");
        SerializedProperty isDefaultValues = enemyData.FindPropertyRelative("UseDefaultValues");
        SerializedProperty enemyType = enemyParameters.FindPropertyRelative("EnemyType");        
        SerializedProperty lifeAmount = enemyParameters.FindPropertyRelative("LifeAmount");
        SerializedProperty aiBehavior = enemyParameters.FindPropertyRelative("AiBehavior");
        SerializedProperty initiative = enemyParameters.FindPropertyRelative("Initiative");

        EditorGUILayout.BeginVertical("button");

        EditorGUILayout.PropertyField(enemyType);
        EditorGUILayout.PropertyField(isDefaultValues);

        if (isDefaultValues.boolValue)
        {
            int enemyTypeIndex = enemyType.enumValueIndex;

            lifeAmount.intValue = myTarget.DefaultStatus.EnemyParameters[enemyTypeIndex].LifeAmount;
            initiative.intValue = myTarget.DefaultStatus.EnemyParameters[enemyTypeIndex].Initiative;
            aiBehavior.objectReferenceValue = myTarget.DefaultStatus.EnemyParameters[enemyTypeIndex].AiBehavior;            
        }
        else
        {
            EditorGUILayout.PropertyField(lifeAmount);
            EditorGUILayout.ObjectField(aiBehavior);
            EditorGUILayout.PropertyField(initiative);
        }

        EditorGUILayout.EndVertical();
    }

    void Init()
    {
        myTarget = (LevelCombatOptions)target;
        mySerializedObject = new SerializedObject(myTarget);
                
        easyCategoryData = mySerializedObject.FindProperty("EasyCombats");
        normalCategoryData = mySerializedObject.FindProperty("NormalCombats");
        hardCategoryData = mySerializedObject.FindProperty("HardCombats");

        titleStyle = new GUIStyle()
        {
            fontSize = 11,
            fontStyle = FontStyle.Bold
        };
    }
}
